import json
import pandas as pd
from dataclasses import dataclass
from typing import List


def normalize_column(
        df: pd.DataFrame,
        column: str,
) -> pd.DataFrame:
    """Заменяет колонку со словарем на несколько колонок.
    Имена новых колонок - ключи словаря
    Значения в новых колонок - значения по заданному ключу в словаре
    """
    return pd.concat(
        [
            df,
            pd.json_normalize(df[column]),
        ],
        axis=1
    ).drop(columns=[column])


def safe_as_list(way: str) -> list:
    """
        Преобразует логги в json в словарь, сохраняет в список
        """
    with open(way) as f:
        POS_CASH_balance = []
        bureau = []
        for line in f:
            line = json.loads(line)
            if line['type'] == 'bureau':
                bureau.append(line['data'])
            else:
                POS_CASH_balance.append(line['data'])
        return bureau, POS_CASH_balance


def df_and_normalize(name_list: List[dict]) -> pd.DataFrame:
    """
   Распарсивает словарь по колонкам для bureau, для POS_CASH - сначала разбивает список словарей с помощью метода explode
   """
    name_list = pd.DataFrame(name_list)
    column_to_parse = name_list.columns[1]
    if column_to_parse == 'records':
        name_list = name_list.explode(column_to_parse).reset_index()
    name_list = normalize_column(name_list, column_to_parse)
    return name_list


def bureu_optimize(bureau: pd.DataFrame) -> pd.DataFrame:
    """
    Заменяет колонку с объектом на колонки с аттрибутами объекта
     """
    bureau['AmtCredit'] = bureau['AmtCredit'].apply(lambda x: eval(x))
    bureau['CREDIT_CURRENCY'] = bureau['AmtCredit'].apply(lambda x: x.CREDIT_CURRENCY)
    bureau['AMT_CREDIT_MAX_OVERDUE'] = bureau['AmtCredit'].apply(lambda x: x.AMT_CREDIT_MAX_OVERDUE)
    bureau['AMT_CREDIT_SUM'] = bureau['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM)
    bureau['AMT_CREDIT_SUM_DEBT'] = bureau['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM_DEBT)
    bureau['AMT_CREDIT_SUM_LIMIT'] = bureau['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM_LIMIT)
    bureau['AMT_CREDIT_SUM_OVERDUE'] = bureau['AmtCredit'].apply(lambda x: x.AMT_CREDIT_SUM_OVERDUE)
    bureau['AMT_ANNUITY'] = bureau['AmtCredit'].apply(lambda x: x.AMT_ANNUITY)
    bureau.drop('AmtCredit', axis='columns', inplace=True)
    return bureau


def POS_CASH_balance_optimize(df: pd.DataFrame) -> pd.DataFrame:
    """
    Заменяет колонку с объектом на колонки с аттрибутами объекта
     """
    df['PosCashBalanceIDs'] = df['PosCashBalanceIDs'].apply(lambda x: eval(x))
    df['SK_ID_PREV'] = df['PosCashBalanceIDs'].apply(lambda x: x.SK_ID_PREV)
    df['SK_ID_CURR'] = df['PosCashBalanceIDs'].apply(lambda x: x.SK_ID_CURR)
    df['NAME_CONTRACT_STATUS'] = df['PosCashBalanceIDs'].apply(lambda x: x.NAME_CONTRACT_STATUS)
    df.drop('PosCashBalanceIDs', axis='columns', inplace=True)
    return df


@dataclass
class PosCashBalanceIDs:
    SK_ID_PREV: int
    SK_ID_CURR: int
    NAME_CONTRACT_STATUS: str


@dataclass
class AmtCredit:
    CREDIT_CURRENCY: str
    AMT_CREDIT_MAX_OVERDUE: float
    AMT_CREDIT_SUM: float
    AMT_CREDIT_SUM_DEBT: float
    AMT_CREDIT_SUM_LIMIT: float
    AMT_CREDIT_SUM_OVERDUE: float
    AMT_ANNUITY: float


def main(
    way_to_log='POS_CASH_balance_plus_bureau-001.log',
    way_to_save_bureau='bureau.csv',
    way_to_save_POS_CASH='POS_CASH_balance.csv',
):
    bureau, POS_CASH_balance = safe_as_list(way_to_log)
    bureau = df_and_normalize(bureau)
    bureau = bureu_optimize(bureau)
    POS_CASH_balance = df_and_normalize(POS_CASH_balance)
    POS_CASH_balance = POS_CASH_balance_optimize(POS_CASH_balance)
    bureau.to_csv(way_to_save_bureau)
    POS_CASH_balance.to_csv(way_to_save_POS_CASH)


main()
