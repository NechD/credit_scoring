Логистическая регрессия, показатели качества:

precision: 0.3333333333333333
 recall : 0.0004850248575239481
 f1_score: 0.000968640271219276
 accuracy_score: 0.9186925373722642
Подбор параметров:
tuned hpyerparameters :(best parameters)  {'C': 0.001, 'penalty': 'l1'}

Desicion tree:
 precision: 0.22262552934059285
 recall : 0.044622286892203225
 f1_score: 0.07434343434343434
 accuracy_score: 0.9096956020457434
Подбор параметров:
DecisionTreeClassifier(max_depth=15, min_samples_leaf=15, min_samples_split=10)
