import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sys
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor, plot_tree
from src.app.utils.database_connect import send_sql_query, get_df_from_query
from src.config.db_args import *
import pickle
from sklearn.pipeline import Pipeline

sys.path.insert(0, r"..\..\src\app\utils")
sys.path.insert(0, r"..\..\src\config")


def get_target_and_connect(dataset_to_connect):
    query = """
    SELECT *
    FROM application_train
    """
    ds_with_target = get_df_from_query(query, DB_ARGS)
    ds_with_target.set_index('sk_id_curr', inplace=True)
    dataset_to_connect = dataset_to_connect.merge(ds_with_target[['target']], how='left', left_index=True,
                                                  right_index=True)
    return dataset_to_connect


def get_scores(y_truely, y_predict):
    """
    Выводит на печать основные метрики модели
    :param y_truely:
    :param y_predict:
    :return:
    """
    print(f' precision: {metrics.precision_score(y_truely, y_predict)}')
    print(f' recall : {metrics.recall_score(y_truely, y_predict)}')
    print(f' f1_score: {metrics.f1_score(y_truely, y_predict)}')
    print(f' accuracy_score: {metrics.accuracy_score(y_truely, y_predict)}')


def save_pickle(
        name_model: str,
        model,
):
    """
    cохраняет модель в pickle файл
    """
    with open(f'{name_model}.pkl', "wb") as f:
        pickle.dump(model, f)


def main(file_1=r'../../my_remote_store/application_train_features.csv',
         file_2=r'../../my_remote_store/_previous_application_final.csv',
         file_3=r'../../my_remote_store/_bureau_features.csv',
         file_4=r'../../my_remote_store/_credit_card_balance_features.csv',
         file_5=r'../../my_remote_store/_installments_payments_feautures.csv'):
    DB_ARGS['user'] = 'postgres'
    DB_ARGS['password'] = '1234'

    df_new = pd.read_csv(file_1)
    for i in [file_2,
              file_3,
              file_4,
              file_5]:
        intermediate = pd.read_csv(i)
        df_new = df_new.merge(intermediate, how='left', on='sk_id_curr')
    df_new = df_new.fillna(0)
    df_new.set_index('sk_id_curr', inplace=True)
    df_new = get_target_and_connect(df_new)
    train_y = df_new['target']
    train_X = df_new.drop(columns='target')
    pipe = Pipeline([('scaler', StandardScaler()),
                     ('logreg', LogisticRegression(penalty='l1', C=1, solver='liblinear'))])
    pipe.fit(train_X, train_y)
    coefficients = pd.concat([pd.DataFrame(train_X.columns), pd.DataFrame(np.transpose(pipe[1].coef_))], axis=1)
    coefficients.columns = ['feauture', 'coef']
    meaningful_feautures = coefficients[coefficients.coef != 0].feauture.to_list()
    x_log_train = train_X[meaningful_feautures].copy()
    # для последующего сравнения качества моделей создадим отложенную выборку
    X_train_o, X_test_o, y_train_o, y_test_o = train_test_split(x_log_train, train_y, test_size=0.33,
                                                                random_state=42)
    param_grid = {"logreg__C": np.logspace(-3, 3, 3), "logreg__penalty": ["l1", "l2"]}
    grid_search = GridSearchCV(pipe, param_grid=param_grid, refit=True)
    grid_search.fit(X_train_o, y_train_o)
    y_pred = grid_search.predict(X_test_o)
    get_scores(y_test_o, y_pred)
    tree = DecisionTreeClassifier()
    tree.fit(train_X, train_y)
    coefficients_tree = pd.concat(
        [pd.DataFrame(train_X.columns), pd.DataFrame(np.transpose(tree.feature_importances_))], axis=1)
    coefficients_tree.columns = ['feauture', 'coef']
    meaningful_feautures_tree = coefficients_tree[coefficients_tree.coef != 0].feauture.to_list()
    x_tree = train_X[meaningful_feautures_tree].copy()
    X_train_t, X_test_t, y_train_t, y_test_t = train_test_split(x_tree, train_y, test_size=0.33, random_state=42)
    grid_tree = {"max_depth": [15, 20, 30], "min_samples_leaf": [5, 10, 15], 'min_samples_split': [5, 10, 15]}
    tree_cv = GridSearchCV(DecisionTreeClassifier(), grid_tree, cv=3)
    tree_cv.fit(X_train_t, y_train_t)
    best_tree = tree_cv.best_estimator_
    best_tree.fit(X_train_t, y_train_t)
    y_predict = best_tree.predict(X_test_t)
    get_scores(y_test_t, y_predict)
    save_pickle('best_tree', best_tree)
    save_pickle('grid_search', grid_search)


main()
