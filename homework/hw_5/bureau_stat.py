import sys
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store")

from db_args import *
from check_stat_sign_func import check_stat_significance

def main(
        file_path_input_m: str,
        file_path_output_m: str,
        cat_features_m: list,
):
    a = file_path_input_m
    b = file_path_output_m
    c = cat_features_m
    check_stat_significance(
        file_path_input=a,
        file_path_output=b,
        cat_features=c)


main(file_path_input_m='bureau_final.csv',
     file_path_output_m='bureau_output_stat.csv',
     cat_features_m=[])
