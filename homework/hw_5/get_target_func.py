import sys
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
from db_args import *
from database_connect import send_sql_query, get_df_from_query

DB_ARGS['user'] = 'postgres'
DB_ARGS['password'] = '1234'

def get_target_and_connect(dataset_to_connect):
    """"
    В функцию передается датасет к которому требуется присоединить таргет.
    На выходе получаем датасет с таргетом.
    """
    query = """
    SELECT *
    FROM application_train
    """
    ds_with_target = get_df_from_query(query, DB_ARGS)
    ds_with_target.set_index('sk_id_curr', inplace=True)
    dataset_to_connect = dataset_to_connect.merge(ds_with_target[['target']], how='left', left_index=True,
                                                  right_index=True)
    return dataset_to_connect