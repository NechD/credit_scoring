import sys
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")

import pandas as pd
import numpy as np
from fill_curr_distr_func import fill_with_currest_distr
from scipy.stats import chi2
from scipy.stats import anderson
from scipy.stats import mannwhitneyu
from scipy.stats import ttest_ind
from scipy.stats import chi2_contingency
from get_target_func import get_target_and_connect


def normal_not_normal(
        data_frame_input: pd.DataFrame) -> tuple:
    """
    Проверяет датафрейм с числовыми признаками на нормальность их распределения.
    Возвращает 2 списка с признаками: с нормально и ненормально распределенными
    """
    feat_norm = []
    feat_not_norm = []
    for i in data_frame_input.columns:
        if i != 'target':
            stat, crit_val, sign_level = anderson(data_frame_input[data_frame_input['target'] == 0][i], 'norm')
            if stat >= crit_val[4]:
                print(f'Гипотеза о согласии с нормальным  распределением признака {i} отвергается. ')
                feat_not_norm.append(i)
            else:
                print(f'Гипотеза о согласии с нормальным распределением признака {i} не отвергается. ')
                feat_norm.append(i)
    return feat_norm, feat_not_norm


def mann_whitney(
        input_df: pd.DataFrame,
        sign: list
) -> list:
    """
    Проверяет значимость числовых признаков, распределенных ненормально по критерию Манна-Уитни,
    input_df: датафрейм с числовыми признаками, распределенными ненормально
    """
    print('in_not_normal')
    for i in input_df:
        if i != 'target':
            _, p_mw = mannwhitneyu(input_df[input_df['target'] == 0][i], input_df[input_df['target'] == 1][i])
            if p_mw < 0.05:
                print(
                    f'p-value для критерия Манна-Уитни: {p_mw:0.4f} < 0.05. Гипотеза об отсутствии различий между группами отвергается. Признак {i} значимый.')
                sign.append(i)
            elif p_mw >= 0.05:
                print(
                    f'p-value для критерия Манна-Уитни: {p_mw:0.4f} >= 0.05. Гипотеза об отсутствии различий между группами не отвергается. Признак {i} незначимый.')
    return sign


def t_test(
        input_df: pd.DataFrame,
        sign: list
) -> list:
    """
    Проверяет значимость числового признака, распределенного нормально
    """
    for i in input_df:
        if i != 'target':
            _, p_tt = ttest_ind(input_df[input_df['target'] == 0][i], input_df[input_df['target'] == 1][i])
            if p_tt < 0.05:
                print(
                    f'p-value для t-test: {p_tt:0.4f} < 0.05. Гипотеза об отсутствии различий между группами отвергается.'
                    f' Признак {i} значимый.')
                sign.append(i)
            elif p_tt >= 0.05:
                print(
                    f'p-value для t-test:  {p_tt:0.4f} >= 0.05. Гипотеза об отсутствии различий между группами не отвергается. '
                    f'Признак {i} незначимый.')
    return sign


def chi_square(
        input_df: pd.DataFrame,
        sign=[],
        prob=0.95
) -> list:
    """
    Проверяет значимость категориальных признаков, возвращает список значимых
    """
    for i in input_df:
        if i != 'target':
            cross_tab = pd.concat([
                pd.crosstab(input_df[i], input_df['target'], margins=False),
                input_df.groupby(i)['target'].agg(['count', 'mean']).round(4)
            ], axis=1).rename(columns={0: f"target=0", 1: f"target=1", "mean": 'probability_of_default'})

            cross_tab['probability_of_default'] = np.round(cross_tab['probability_of_default'] * 100, 2)
            chi2_stat, p, dof, expected = chi2_contingency(cross_tab.values)
            critical = chi2.ppf(prob, dof)
            if abs(chi2_stat) >= critical:
                print(f'Гипотеза об отсутствии различий между группами отвергается. Признак {i} значимый.')
                sign.append(i)
            else:
                print(f'Гипотеза об отсутствии различий между группами не отвергается. Признак незначимый.')
    return sign


def check_stat_significance(
        file_path_input: str,
        cat_features: list,
        file_path_output='bureau_stat_features.csv',
):
    """
    Проверяет датасет на значимость признаков и оставляет только статистически значимые
    """
    df = pd.read_csv(file_path_input, index_col=0)
    num_features = [i for i in df.columns if i not in cat_features]
    df_cat = df[cat_features].copy()
    df_num = df[num_features].copy()
    feat_norm, feat_not_norm = [], []
    print(df_num.empty)
    if not df_num.empty:
        df_num = fill_with_currest_distr(df_num)
        df_num = get_target_and_connect(df_num).copy()
        feat_norm, feat_not_norm = normal_not_normal(df_num)
    print(df_cat.empty)
    if not df_cat.empty:
        df_cat = fill_with_currest_distr(df_cat)
        df_cat = get_target_and_connect(df_cat).copy()
    sign = []
    if feat_norm:
        feat_norm.append('target')
    if feat_not_norm:
        feat_not_norm.append('target')
    sign = mann_whitney(df_num[feat_not_norm], sign)
    sign = t_test(df_num[feat_norm], sign)
    sign = chi_square(df_cat, sign)
    df_tosave = df[sign]
    df_tosave.to_csv(file_path_output)
