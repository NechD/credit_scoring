
import sys
from check_stat_sign_func import check_stat_significance

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")



def main(
        file_path_input_m: str,
        file_path_output_m: str,
        cat_features_m: list,
):
    a = file_path_input_m
    b = file_path_output_m
    c = cat_features_m
    check_stat_significance(
        file_path_input=a,
        file_path_output=b,
        cat_features=c)


main(file_path_input_m='previous_application_final.csv',
     cat_features_m=[],
     file_path_output_m='previous_application_stat.csv')