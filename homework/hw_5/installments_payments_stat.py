
import sys


sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
from database_connect import send_sql_query, get_df_from_query
from db_args import *

from check_stat_sign_func import check_stat_significance

DB_ARGS['user'] = 'postgres'
DB_ARGS['password'] = '1234'



def main(
        file_path_input_m: str,
        file_path_output_m: str,
        cat_features_m: list,
):
    a = file_path_input_m
    b = file_path_output_m
    c = cat_features_m
    check_stat_significance(
        file_path_input=a,
        file_path_output=b,
        cat_features=c)


main(
    file_path_input_m='installments_payments_feautures.csv',
    file_path_output_m='installments_payments_stat.csv',
    cat_features_m=[])