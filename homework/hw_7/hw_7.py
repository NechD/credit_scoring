import pandas as pd
import pickle
from catboost import CatBoostClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.ensemble import StackingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from catboost import Pool, EShapCalcType, EFeaturesSelectionAlgorithm


def get_scores(
        y_truely,
        y_predict,
        y_predict_proba
):
    """
    Выводит на печать основные метрики модели
    :param y_truely:
    :param y_predict:
    :return:
    """
    print(f' precision: {metrics.precision_score(y_truely, y_predict)}')
    print(f' recall : {metrics.recall_score(y_truely, y_predict)}')
    print(f' f1_score: {metrics.f1_score(y_truely, y_predict)}')
    print(f' accuracy_score: {metrics.accuracy_score(y_truely, y_predict)}')
    print(f' roc_auc_score: {metrics.roc_auc_score(y_truely, y_predict_proba)}')


def save_pickle(
        name_model: str,
        model
):
    """
    cохраняет модель в pickle файл
    """
    with open(f"model_{name_model}.pkl", "wb") as f:
        pickle.dump(model, f)


def main(input_file="final_dateset.csv"):
    df = pd.read_csv(input_file, index_col=0)
    X = df.drop(columns='target')
    y = df.target
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
    feature_names = X_train.columns.to_list()
    train_pool = Pool(X_train, y_train, feature_names=feature_names)
    test_pool = Pool(X_test, y_test, feature_names=feature_names)
    # Создаем словарь моделей, которые будем использовать
    estimatorss = {'RandomForestClassifier': RandomForestClassifier(max_depth=25,
                                                                    n_estimators=50,
                                                                    random_state=42,
                                                                    max_features=0.7),
                   'CatBoost': CatBoostClassifier(random_state=42),
                   'DecisionTree': DecisionTreeClassifier(max_depth=15,
                                                          min_samples_leaf=15,
                                                          min_samples_split=10),
                   'LogisticRegression': LogisticRegression(C=0.001,
                                                            penalty='l1',
                                                            solver='liblinear'),
                   'stacking': '',
                   'CatBoost_2': CatBoostClassifier(random_state=42)}
    for new_estimator in estimatorss:
        print(new_estimator)
        # Инициализируем модель
        new_model = estimatorss[new_estimator]
        if new_estimator not in ['LogisticRegression', 'stacking']:
            if new_estimator == 'CatBoost_2':
                # попробуем отобрать признаки с помощью встроенного метода Catboost и посмотрим на качество
                summary = new_model.select_features(
                    train_pool,
                    eval_set=test_pool,
                    features_for_select='0-161',
                    num_features_to_select=50,
                    steps=3,
                    algorithm=EFeaturesSelectionAlgorithm.RecursiveByShapValues,
                    shap_calc_type=EShapCalcType.Regular,
                    train_final_model=True,
                    logging_level='Silent'
                )
                best_features = summary['selected_features_names']
                X_train = X_train[best_features]
                X_test = X_test[best_features]
                new_model = estimatorss[new_estimator]
        # Для логистической регрессии построим простой Пайплайн: скалирование и потом модель
        elif new_estimator == 'LogisticRegression':
            scaler = StandardScaler()
            new_model = Pipeline(steps=[("scaler", scaler), ("logistic", new_model)])
        else:
        # Реализуем стэкинг базовых алогоиртмов
            estimators = [
                ('rf', RandomForestClassifier(max_depth=25, n_estimators=50, random_state=42, max_features=0.7)),
                ('cb', CatBoostClassifier(random_state=42, early_stopping_rounds=30)),
                ('svr', make_pipeline(StandardScaler(), LinearSVC(random_state=42)))]
            new_model = StackingClassifier(estimators=estimators,
                                           final_estimator=LogisticRegression(C=0.001,
                                                                              penalty='l1',
                                                                              solver='liblinear',
                                                                              max_iter=250))
        new_model.fit(X_train, y_train)
        get_scores(y_test, new_model.predict(X_test), new_model.predict_proba(X_test)[:, 1])
        save_pickle(new_estimator, new_model)


main()
