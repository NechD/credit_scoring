import sys
import pandas as pd
import numpy as np

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store")
from database_connect import send_sql_query, get_df_from_query
from db_args import *

DB_ARGS['user'] = 'postgres'
DB_ARGS['password'] = '1234'


def main(dataset_name,
         path_to_save=r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store"):
    query = f"""
    SELECT *
    FROM {dataset_name}
    """
    name_for_saving = dataset_name
    dataset_name = get_df_from_query(query, DB_ARGS)

    # количество документов
    doc_col = [col for col in dataset_name if col.startswith('flag_document')]
    dataset_name['num_doc'] = dataset_name[doc_col].sum(axis=1)

    # Есть ли полная информация о доме.
    dataset_name['inf_house'] = (
            dataset_name.loc[:, 'apartments_avg':'emergencystate_mode'].isna().sum(axis=1) < 30).astype(int)

    # Кол-во полных лет
    dataset_name['full_age'] = np.ceil(dataset_name['days_birth'] * -1 / 365)

    # Возраст смены документа
    dataset_name['document_change_age'] = np.ceil(
        (dataset_name['days_birth'] - dataset_name['days_id_publish']) * -1 / 365)

    # Разница во времени между сменой документа и возрастом на момент смены документы
    dataset_name['full_age_from_doc_change'] = dataset_name['full_age'] - dataset_name['document_change_age']
    # Признак задержки смены документа. Документ выдается или меняется в 14, 20 и 45 лет, 1 - есть задержка
    dataset_name['document_change_is_delay'] = (
        ~dataset_name['document_change_age'].isin([14, 20, 45])).astype(int)

    # Доля денег которые клиент отдает на займ за год
    dataset_name['part_income_for_loan'] = (dataset_name['amt_annuity'] * 12) / (
            dataset_name['amt_income_total'] * 12)

    # Кол-во детей в семье на одного взрослого
    dataset_name['children_per_adult'] = dataset_name['cnt_children'] / (
            dataset_name['cnt_fam_members'] - dataset_name['cnt_children'])

    # Доход на ребенка, взрослого
    dataset_name['income_per_child'] = dataset_name['amt_income_total'] / dataset_name['cnt_children']
    dataset_name.replace([np.inf, -np.inf], 0, inplace=True)
    dataset_name['income_per_adult'] = dataset_name['amt_income_total'] / (
            dataset_name['cnt_fam_members'] - dataset_name['cnt_children'])

    # Расчет процентной ставки FV=PV(1+i)^n, отсюда i = -1 + (FV/PV)^1/n., 
    # где FV- future value, PV - present value, n - число периодов ,i - процентная ставка 
    # Для расчета процентной ставки нам нужно число годов
    dataset_name['number_of_years'] = dataset_name['amt_credit'] /  dataset_name['amt_annuity'] / 12
    dataset_name['interest_rate'] = round((-1 + (dataset_name['amt_credit'] / dataset_name['amt_goods_price']) ** (1 / dataset_name['number_of_years'])) * 100, 2)
    dataset_name.drop(columns='number_of_years', inplace=True)
    
    # Взвешенный скор внешних источников. У всех трех фичей отрицательная корреляция с таргетом, при этом все 3 лежат в промежутке от 0 до 1.
    # распределение у source_1 и source_2 приблизительно одинаковое, при это source_2 как будто бы более строкий критерий, максимум ниже
    # поэтому считаю, что ему нужно задать вес чуть выше.
    
    dataset_name['weighted_out'] = (dataset_name['ext_source_1'] + 1.2 * dataset_name['ext_source_2'] + dataset_name[
        'ext_source_3']) / 3


    # (доп признак)
    dataset_name['zapros_chastots'] = dataset_name.amt_req_credit_bureau_hour \
                                   + dataset_name.amt_req_credit_bureau_day / 24 \
                                   + dataset_name.amt_req_credit_bureau_week / (24 * 7) \
                                   + dataset_name.amt_req_credit_bureau_mon / (24 * 7 * 30) \
                                   + dataset_name.amt_req_credit_bureau_qrt / (24 * 7 * 30 * 3) \
                                   + dataset_name.amt_req_credit_bureau_year / (24 * 7 * 30 * 12)

    # Поделим людей на группы в зависимости от пола и образования.
    x = dataset_name.groupby(['full_age', 'code_gender'], as_index=False) \
        .agg({'amt_income_total': np.mean}) \
        .rename({'amt_income_total': 'mean_income_group'}, axis='columns')
    dataset_name = dataset_name.merge(x, on=['full_age', 'code_gender'], how='left')
    dataset_name['difference_group_minus_personal'] = dataset_name['mean_income_group'] - dataset_name[
        'amt_income_total']
    dataset_name.drop('mean_income_group', axis='columns', inplace=True)
    dataset_name.set_index('sk_id_curr', inplace=True)
    dataset_name.columns = dataset_name.columns.str.lower()
    dataset_name_save = dataset_name.loc[:, 'num_doc':]
    dataset_name_save.to_csv(f'{path_to_save}/\{name_for_saving}_features.csv')

main('application_train')
main('application_test')