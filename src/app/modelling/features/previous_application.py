import sys
import pandas as pd
import numpy as np

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store")
from database_connect import send_sql_query, get_df_from_query
from db_args import *


def main(path_to_save=r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store"):
    DB_ARGS['user'] = 'postgres'
    DB_ARGS['password'] = '1234'
    query = """
    SELECT *
    FROM previous_application
    """
    previous_application = get_df_from_query(query, DB_ARGS)
    p_ap = previous_application.pivot_table(index='sk_id_curr',
                                            columns='name_contract_type',
                                            values='amt_credit',
                                            aggfunc=['sum', 'mean', 'min', 'max']).fillna(0)
    p_ap.columns = p_ap.columns.map('_'.join)
    p_ap = p_ap.reset_index()
    p_count = previous_application.groupby(['sk_id_curr', 'name_contract_type'], as_index=False) \
        .agg({'sk_id_prev': 'count'}) \
        .pivot(index='sk_id_curr', columns='name_contract_type', values='sk_id_prev').fillna(0)
    p_count = p_count.reset_index()
    previous_application_final = p_ap.merge(p_count, on=['sk_id_curr'], how='left')
    previous_application_final.set_index('sk_id_curr', inplace=True)
    print(previous_application_final.columns)
    previous_application_final.columns = previous_application_final.columns.str.replace(' ', '_').str.lower()
    print(previous_application_final.columns)
    previous_application_final.to_csv(path_to_save + '\\' + '_previous_application_final.csv')



main()
