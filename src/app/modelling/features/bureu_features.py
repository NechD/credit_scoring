import sys
import pandas as pd
import numpy as np

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store")
from database_connect import send_sql_query, get_df_from_query
from db_args import *


def main(path_to_save=r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store"):
    DB_ARGS['user'] = 'postgres'
    DB_ARGS['password'] = '1234'
    query = """
    SELECT *
    FROM bureau_balance
    """
    bureau_balance = get_df_from_query(query, DB_ARGS)

    query = """
    SELECT *
    FROM bureau
    """
    bureau = get_df_from_query(query, DB_ARGS)

    # Кол-во открытых, закрытых, просроченных кредитов
    # находим по каждому кредиту последнюю активную дату
    idx = bureau_balance.groupby(['sk_id_bureau'])['months_balance'].transform(max) == bureau_balance['months_balance']
    # берем строку с последней активной датой по каждому кредиту, в т.ч. статус
    prom = bureau_balance[idx]
    bureau_united = bureau.merge(prom, on='sk_id_bureau', how='left')
    # создаем пивот таблицу, в которой для каждого пользователя получим число кредитов по всем категориям
    # на последнюю дату
    bureau_final = bureau_united.groupby(['sk_id_curr', 'status'], as_index=False) \
        .agg({'sk_id_bureau': 'count'}) \
        .rename(columns={'sk_id_bureau': 'number_in_category'}) \
        .pivot(index='sk_id_curr', columns='status', values='number_in_category').reset_index() \
        .rename(columns={'C': 'closed_credits'}).fillna(0)
    rename_col = {'1': '1-30_delay',
                  '2': '31-60_delay',
                  '3': '61-90_delay',
                  '4': '91-120_delay',
                  '5': '121_delay'}
    bureau_final.rename(columns=rename_col, inplace=True)
    bureau_final['opened_credits'] = bureau_final.loc[:, '0':'121_delay'].sum(axis=1)
    bureau_final['total_credits'] = bureau_final.loc[:, '0':'X'].sum(axis=1)
    
    # Доля открытых кредитов, и по дням просрочки
    for i in ['opened_credits',
              'closed_credits',
              '1-30_delay',
              '31-60_delay',
              '61-90_delay',
              '91-120_delay',
              '121_delay']:
        bureau_final[f'{i}_pr'] = bureau_final[i].divide(bureau_final["total_credits"], fill_value=0)


    # Интервал между последним закрытым кредитом и текущей заявкой
    # отбираем только закрытые кредиты
    only_closed_credits = bureau_balance.query('status == "C"').copy()
    # для каждой закрытой заявки отбираем последнюю дату
    idx2 = (only_closed_credits.groupby(['sk_id_bureau'])['months_balance'] \
                               .transform(max)) == (only_closed_credits['months_balance'])
    bureau_sk_last_month = only_closed_credits[idx2].copy()
    bureau_sk_last_month.rename(columns={'months_balance': 'closed_months_ago'}, inplace=True)
    bureau_united = bureau_united.merge(bureau_sk_last_month[['sk_id_bureau', 'closed_months_ago']], how='left', on=['sk_id_bureau'])
    # находим для каждого пользователя как давно он закрыл самый последний(тот, который закрыт недавно) кредит
    bureau_interval_last = bureau_united.groupby('sk_id_curr', as_index=False).agg({'closed_months_ago': 'max'})
    bureau_final = bureau_final.merge(bureau_interval_last, how='left', on=['sk_id_curr'])

    # Интервал между взятием последнего активного займа и текущей заявкой
    # по каждому кредиту находим последнюю активную дату(чтобы понять, закрыт он или нет)
    lal = (bureau_balance.groupby(['sk_id_bureau'])['months_balance'] \
                         .transform(max)) == (bureau_balance['months_balance'])
    # отбираем открытые кредиты и получаем их id
    number_active_credits = bureau_balance[lal].query('status != "C"')['sk_id_bureau']
    # получаем всю информацию об активных кредитах
    full_active_credits = bureau_balance.loc[bureau_balance.sk_id_bureau.isin(number_active_credits)]
    # для каждого кредита получаем дату, когда кредит был открыт
    when_credit_was_taken = full_active_credits.groupby('sk_id_bureau', as_index=False) \
        .agg({'months_balance': 'min'}) \
        .rename(columns={'months_balance': 'first_date_credit_was_taken'})
    bureau_united = bureau_united.merge(when_credit_was_taken, how='left', on=['sk_id_bureau'])
    # для каждого пользователя получаем дату, когда последний активный кредит был взят
    sk_id_last_active_credit_taken = bureau_united.groupby('sk_id_curr', as_index=False)\
                                                  .agg({'first_date_credit_was_taken': 'max'})
    bureau_final = bureau_final.merge(sk_id_last_active_credit_taken, how='left', on=['sk_id_curr'])
    bureau_final.rename(columns={'first_date_credit_was_taken': 'last_andnow_active_c_was_taken'}, inplace=True)


    # максимальная, мин. сумма просрочки
    amt_credit_max_overdue = bureau_united.groupby('sk_id_curr', as_index=False) \
                                          .agg({'amt_credit_max_overdue': ['max', 'min']})
    amt_credit_max_overdue.columns = amt_credit_max_overdue.columns.map('_'.join)
    amt_credit_max_overdue.rename(columns={'sk_id_curr_': 'sk_id_curr'}, inplace=True)
    bureau_final.merge(amt_credit_max_overdue, on=['sk_id_curr'], how='left')

    # Какую долю суммы от открытого займа просрочил
    # Кол-во кредитов определенного типа, смержить
    credit_type_id = bureau_united.groupby(['sk_id_curr', 'credit_type'], as_index=False) \
        .agg({'sk_id_bureau': 'count'}) \
        .rename(columns={'sk_id_bureau': 'number_in_credit_type'}) \
        .pivot(index='sk_id_curr', columns='credit_type', values='number_in_credit_type') \
        .reset_index()\
        .fillna(0)
    bureau_final = bureau_final.merge(credit_type_id, on=['sk_id_curr'], how='left')

    # Кол-во просрочек кредитов определенного типа
    bureau_united['is_there_delay'] = bureau_united.status.isin(['1', '2', '3', '4', '5']).astype(int)
    users_delays_by_type = bureau_united.groupby(['sk_id_curr', 'credit_type'], as_index=False) \
        .agg({'is_there_delay': 'sum'}) \
        .rename(columns={'is_there_delay': 'count_delay'}) \
        .pivot(index='sk_id_curr', columns='credit_type', values='count_delay').fillna(0)
    bureau_final = bureau_final.merge(users_delays_by_type, on=['sk_id_curr'], how='left')

    # Кол-во закрытых кредитов определенного типа
    bureau_united['is_closed'] = bureau_united.status.isin(['C']).astype(int)
    users_closed_by_type = bureau_united.groupby(['sk_id_curr', 'credit_type'], as_index=False) \
        .agg({'is_closed': 'sum'}) \
        .rename(columns={'is_closed': 'count_closed_type'}) \
        .pivot(index='sk_id_curr', columns='credit_type', values='count_closed_type').fillna(0)
    bureau_final = bureau_final.merge(users_closed_by_type, on=['sk_id_curr'], how='left')
    bureau_final.set_index('sk_id_curr', inplace=True)
    bureau_final.columns = bureau_final.columns.str.lower()
    bureau_final.to_csv(f'{path_to_save}/\_bureau_features.csv')


main()
