import sys
import pandas as pd
import numpy as np
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store")

from database_connect import send_sql_query, get_df_from_query
from db_args import *

def main(path_to_save=r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store"):
    DB_ARGS['user'] = 'postgres'
    DB_ARGS['password'] = '1234'
    query = """
    SELECT *
    FROM installments_payments
    """
    installments_payments = get_df_from_query(query, DB_ARGS)
    installments_payments['diff_expected_observed_payment_days'] = installments_payments['days_entry_payment'] - \
                                                                   installments_payments['days_instalment']
    installments_payments['diff_expected_observed_payment_amt'] = installments_payments['amt_payment'] - \
                                                                  installments_payments['amt_instalment']
    installments_payments['payment_less'] = (installments_payments.diff_expected_observed_payment_amt < 0).astype(int)
    installments_payments['payment_more'] = (installments_payments.diff_expected_observed_payment_amt > 0).astype(int)
    installments_payments['payment_later'] = (installments_payments.diff_expected_observed_payment_days < 0).astype(int)
    installments_payments['payment_earlier'] = (installments_payments.diff_expected_observed_payment_days > 0).astype(
        int)
    installments_payments_feautures = installments_payments.groupby('sk_id_curr', as_index=False).agg(
        {'payment_less': 'mean',
         'payment_more': 'mean',
         'payment_later': 'sum',
         'payment_earlier': 'sum',
         'amt_payment': ['mean','median'],
         'diff_expected_observed_payment_days': ['mean', 'max', 'min', 'median'],
         'diff_expected_observed_payment_amt': ['mean', 'max', 'min', 'median']})
    installments_payments_feautures.columns = installments_payments_feautures.columns.map('_'.join)
    installments_payments_feautures.rename(columns={'sk_id_curr_': 'sk_id_curr'}, inplace=True)
    installments_payments_feautures.set_index('sk_id_curr', inplace=True)
    installments_payments_feautures.columns = installments_payments_feautures.columns.str.lower()
    installments_payments_feautures.to_csv(path_to_save + '\\'+'_installments_payments_feautures.csv')

main()
