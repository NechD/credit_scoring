import sys
import pandas as pd
import numpy as np

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store")

from database_connect import send_sql_query, get_df_from_query
from db_args import *


def main(path_to_save=r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store"):
    DB_ARGS['user'] = 'postgres'
    DB_ARGS['password'] = '1234'
    query = """
    SELECT *
    FROM credit_card_balance
    """
    credit_card_balance = get_df_from_query(query, DB_ARGS)

    # создаем список агрегирующих функций
    agg_functions = ['min', 'mean', 'max']
    # получаем список колонок, по которым можно посчитать агрегаты
    agg_col = [col for col in credit_card_balance if col.startswith(('amt', 'cnt'))]
    # с помощью list_comprehension генерируем словарь для каждой колонки прим. агр. функции
    credit_card_balance_final = credit_card_balance.groupby('sk_id_curr') \
        .agg({i: agg_functions for i in agg_col}).fillna(0)
    credit_card_balance_final.columns = credit_card_balance_final.columns.map('_'.join)
    credit_card_balance_final.columns = credit_card_balance_final.columns.str.lower()
    credit_card_balance_final.to_csv(f'{path_to_save}/\_credit_card_balance_features.csv')


main()
