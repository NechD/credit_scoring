import pandas as pd
import sys
from catboost import CatBoostClassifier
from sklearn.model_selection import train_test_split
from catboost import Pool, EShapCalcType, EFeaturesSelectionAlgorithm

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
from save_pickle import save_pickle
from get_scores import get_scores


def save_datasets_after_feature_sel_cb(
        input_train_train: str,
        input_train_val: str,
        input_test: str,
        features_to_save: list,
):
    """
    Функция принимает на вход датасеты и фичи и пересохраняет датасеты с отобранными фичами.
    input_train_train - путь до трейн_трейн файла
    input_train_val - путь до трейн_вал файла
    input_test - путь до тестового файла
    features_to_save - фичи, отобранные в ходе какого-то процесса, которые нужно сохранить во всех датасетах
    """
    list_files = {'train_train_cb_sel': input_train_train,
                  'train_val_cb_sel': input_train_val,
                  'test_cb_sel': input_test}
    for dataframe in list_files:
        df_prom = pd.read_csv(list_files[dataframe], index_col=0)
        if dataframe != 'test_cb_sel':
            df_prom = df_prom[features_to_save]
        else:
            fet = [i for i in features_to_save if i != 'target']
            df_prom = df_prom[fet]
        df_prom.to_csv(
            r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store' + '\\' + dataframe + '.csv')
        print(df_prom.columns.to_list())


def main(
        input_file_path=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\train_connected_train.csv',
        input_val_path=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\train_connected_val.csv'
):
    df = pd.read_csv(input_file_path, index_col=0)
    X = df.drop(columns='target')
    y = df.target
    cat_features = ['inf_house', 'document_change_is_delay']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
    feature_names = X_train.columns.to_list()
    train_pool = Pool(X_train,
                      y_train,
                      feature_names=feature_names,
                      cat_features=cat_features)
    test_pool = Pool(X_test,
                     y_test,
                     feature_names=feature_names,
                     cat_features=cat_features)
    cb_clf = CatBoostClassifier(random_state=42,
                                cat_features=cat_features)
    summary = cb_clf.select_features(train_pool,
                                     eval_set=test_pool,
                                     features_for_select='0-119',
                                     num_features_to_select=30,
                                     steps=15,
                                     algorithm=EFeaturesSelectionAlgorithm.RecursiveByShapValues,
                                     shap_calc_type=EShapCalcType.Regular,
                                     train_final_model=True,
                                     logging_level='Silent'
                                     )
    best_features = summary['selected_features_names']
    print(best_features)
    X = X[best_features]
    cat_features = list(set(cat_features) & set(best_features))
    cb_clf_plus = CatBoostClassifier(random_state=42,
                                     cat_features=cat_features)
    cb_clf_plus.fit(X, y)
    df_val = pd.read_csv(input_val_path, index_col=0)
    X_val = df_val.drop(columns='target')
    y_val = df_val.target
    X_val = X_val[best_features]
    get_scores(y_val, cb_clf_plus.predict(X_val), cb_clf_plus.predict_proba(X_val)[:, 1])
    save_pickle('catboost_classifier', cb_clf_plus)
    save_features = best_features + ['target']
    save_datasets_after_feature_sel_cb(
        input_train_train=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\train_connected_train.csv',
        input_train_val=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\train_connected_val.csv',
        input_test=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\test_connected.csv',
        features_to_save=save_features)


main()
