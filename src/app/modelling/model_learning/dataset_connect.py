import sys
import pandas as pd
from sklearn.model_selection import train_test_split

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
from get_target_func import get_target_and_connect


def split_to_val(df: pd.DataFrame):
    """
    Разделяет Датафрейм на тестовый и валидационный
    """
    train_connected_train, train_connected_val = train_test_split(df, test_size=0.2, random_state=42)
    train_connected_train.to_csv(r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store' + '\\' + 'train_connected_train' + '.csv')
    train_connected_val.to_csv(r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store' + '\\' + 'train_connected_val' + '.csv')


def connect_all_datasets(
        name: str,
        path: str
):
    """"
    Соединяет все Датасеты в один, для обучения модели.
    Тренировочный датасет(с таргетом) разделяется на валидационный и тренировочный(80/20)
    """
    df_new = pd.read_csv(path)
    for i in ['previous_application_stat.csv',
              'bureau_features_stat.csv',
              'credit_card_balance_stat.csv',
              'installments_payments_stat.csv']:
        intermediate = pd.read_csv(r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store' + '\\' + i)
        df_new = df_new.merge(intermediate, how='left', on='sk_id_curr')
    df_new = df_new.fillna(0)
    df_new.set_index('sk_id_curr', inplace=True)
    df_new.to_csv(r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store' + '\\' + name + '.csv')

    if 'test' not in name:
        print(name, 'in_train')
        df_new = get_target_and_connect(df_new)
        split_to_val(df_new)


def main(name1: str, path1: str,
         name2: str, path2: str):
    connect_all_datasets(name1, path1)
    connect_all_datasets(name2, path2)


main('train_connected',
     r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\application_train_stat_sign_features.csv',
     'test_connected',
     r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\application_test_stat_sign_features.csv')