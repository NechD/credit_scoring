import sys
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")
from check_stat_sign_func import check_stat_significance


def main(
        file_path_input_m: str,
        file_path_output_m: str,
        cat_features_m: list,
):
    check_stat_significance(
        file_path_input=file_path_input_m,
        file_path_output=file_path_output_m,
        cat_features=cat_features_m)


main(
    file_path_input_m=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\_credit_card_balance_features.csv',
    file_path_output_m=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\credit_card_balance_stat.csv',
    cat_features_m=[])
