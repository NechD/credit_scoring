import pandas as pd
import numpy as np


def fill_with_currest_distr(input_df: pd.DataFrame) -> pd.DataFrame:
    """
    Заполняет в DataFrame пропуски с учетом существующего распределения признака
    """
    for i in input_df.columns:
        if input_df[i].isna().sum() > 0:
            s = input_df[i].value_counts(normalize=True)
            missing = input_df[i].isnull()
            input_df.loc[missing, i] = np.random.choice(s.index, size=len(input_df[missing]), p=s.values)
    return input_df
