import sys
from check_stat_sign_func import check_stat_significance
import pandas as pd

sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\config")


def main(
        file_path_input_m: str,
        file_path_input_test: str,
        file_path_output_m: str,
        file_path_output_test: str,
        cat_features_m: list,
):
    check_stat_significance(
        file_path_input=file_path_input_m,
        file_path_output=file_path_output_m,
        cat_features=cat_features_m)
    df_train = pd.read_csv(file_path_output_m, index_col=0)
    good_columns = df_train.columns.to_list()
    print(good_columns)
    df_test = pd.read_csv(file_path_input_test, index_col=0)
    df_test = df_test[good_columns]
    df_test.to_csv(file_path_output_test)


main(
    file_path_input_m=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\application_train_features.csv',
    file_path_input_test=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\application_test_features.csv',
    file_path_output_m=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\application_train_stat_sign_features.csv',
    file_path_output_test=r'C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\my_remote_store\application_test_stat_sign_features.csv',
    cat_features_m=['inf_house', 'document_change_is_delay']
)
