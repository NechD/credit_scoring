from dataclasses import dataclass
from enum import Enum, auto


class ScoringDecision(Enum):
    """Возможные решения модели."""

    accepted = auto()
    declined = auto()


@dataclass
class ScoringResult(object):
    """Класс, содержащий результаты скоринга."""

    decision: ScoringDecision
    amount: int
    threshold: float
    proba: float


@dataclass
class Features(object):
    """Фичи для принятия решения об одобрении."""

    num_doc: float = 0
    full_age: float = 0
    document_change_age: float = 0
    full_age_from_doc_change: float = 0
    part_income_for_loan: float = 0
    income_per_adult: float = 0
    interest_rate: float = 0
    weighted_out: float = 0
    difference_group_minus_personal: float = 0
    # inf_house - категориальная фича
    inf_house: int = 0
    sum_cash_loans: float = 0
    sum_consumer_loans: float = 0
    max_cash_loans: float = 0
    cash_loans: float = 0
    revolving_loans: float = 0
    closed_credits: float = 0
    credit_card_x: float = 0
    microloan_x: float = 0
    amt_credit_limit_actual_min: float = 0
    amt_payment_total_current_mean: float = 0
    amt_total_receivable_mean: float = 0
    cnt_drawings_atm_current_mean: float = 0
    cnt_drawings_current_max: float = 0
    cnt_instalment_mature_cum_mean: float = 0
    payment_less_mean: float = 0
    payment_more_mean: float = 0
    payment_later_sum: float = 0
    payment_earlier_sum: float = 0
    amt_payment_median: float = 0
    diff_expected_observed_payment_amt_min: float = 0
