import pickle

from api import Features, ScoringDecision, ScoringResult
from calculator import Calculator


class MyCoreModel(object):
    """Класс для создания модели и её интерфейса."""

    _threshold = 0.2

    def __init__(self, model_path: str):
        self._calculator = Calculator()
        with open(model_path, 'rb') as pickled_model:
            self._model = pickle.load(pickled_model)

    def get_scoring_result(self, features):
        """
        На вход принимает фичи.
        На выход подает результат проведения кредитного скоринга.
        Решение о выдаче, сумма, порог, вероятность возврата займа.
        """
        probability = self._predict_proba(features)
        final_decision = ScoringDecision.declined
        approved_amount = 0
        # Сatboost возвращает numpy array с принадлежностями класса.
        # Нас интересует вероятность, что клиент хороший.
        probability = probability[0]
        if probability > self._threshold:
            final_decision = ScoringDecision.accepted
            approved_amount = self._calculator.calc_amount(
                probability,
                features,
            )
        return ScoringResult(
            decision=final_decision,
            amount=approved_amount,
            threshold=self._threshold,
            proba=probability,
        )

    def get_scoring_result2(self, features):
        """
        На вход принимает фичи.
        На выход подает результат проведения кредитного скоринга.
        Решение о выдаче, сумма, порог, вероятность возврата займа.
        """
        probability = self._predict_proba(features)
        final_decision = ScoringDecision.declined
        approved_amount = 0
        # Сatboost возвращает numpy array с принадлежностями класса.
        # Нас интересует вероятность, что клиент хороший.
        probability = probability[0]
        if probability > self._threshold:
            final_decision = ScoringDecision.accepted
            approved_amount = self._calculator.calc_amount2(
                probability,
                features,
            )
        return ScoringResult(
            decision=final_decision,
            amount=approved_amount,
            threshold=self._threshold,
            proba=probability,
        )

    def _predict_proba(self, features: Features) -> float:
        """Определяет вероятность возврата займа."""
        # важен порядок признаков для catboost
        return self._model.predict_proba([
            features.num_doc,
            features.full_age,
            features.document_change_age,
            features.full_age_from_doc_change,
            features.part_income_for_loan,
            features.income_per_adult,
            features.interest_rate,
            features.weighted_out,
            features.difference_group_minus_personal,
            features.inf_house,
            features.sum_cash_loans,
            features.sum_consumer_loans,
            features.max_cash_loans,
            features.cash_loans,
            features.revolving_loans,
            features.closed_credits,
            features.credit_card_x,
            features.microloan_x,
            features.amt_credit_limit_actual_min,
            features.amt_payment_total_current_mean,
            features.amt_total_receivable_mean,
            features.cnt_drawings_atm_current_mean,
            features.cnt_drawings_current_max,
            features.cnt_instalment_mature_cum_mean,
            features.payment_less_mean,
            features.payment_more_mean,
            features.payment_later_sum,
            features.payment_earlier_sum,
            features.amt_payment_median,
            features.diff_expected_observed_payment_amt_min,
        ],
        )
