from src.app.core.api import Features


class Calculator(object):
    """Класс калькулятора."""

    standart_sum = 100000

    def calc_amount(
        self,
        proba: float,
        features: Features,
    ) -> int:
        """
        Метод максимизации прибыли.
        Принимает на вход вероятность возврата, признаки, расчитывает одобренную сумму.
        Первоначальная сумма при вероятности возврата 100% - 100 тысяч.
        Для расчета функции будет использована обычная квадратичная функция y=x^2.
        Если вероятность возврата низка - сумма одобряемого кредита резко сокращается.
        Чем выше вероятность, тем выше одобряемая от 100 тыс. сумма.
        P(0.5) = 25 тыс, 0.6 = 36 тыс, 0.7 = 49 тыс, 0.8 = 64 тыс.
        """
        return self.standart_sum * (proba**2)


    def calc_amount2(self, proba: float, features: Features) -> int:
        """
        Добавлен более сложный калькулятор, чтобы можно было далее провести тесты
        """
        border_normal_person = 25
        border_young_person = 18
        border_normal_person_high = 60
        normal_number_documents = 3

        if border_normal_person < features.full_age <= border_normal_person_high:
            if features.num_doc >= normal_number_documents:
                if features.inf_house == 1:
                    standart_sum = 200000
                else:
                    standart_sum = 150000
            else:
                standart_sum = 125000
        elif border_young_person <= features.full_age <= border_normal_person:
            if features.num_doc >= 3:
                if features.inf_house == 1:
                    standart_sum = 125000
                else:
                    standart_sum = 100000
            else:
                standart_sum = 50000
        else:
            standart_sum = 25000

        return standart_sum * (proba ** 2)
