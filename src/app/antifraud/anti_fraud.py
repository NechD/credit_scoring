import pandas as pd
import numpy as np
import sys
from catboost import CatBoostClassifier
sys.path.insert(0, r"C:\Users\reieg\OneDrive\Рабочий стол\ЦФТ\Репозиторий\credit_scoring\src\app\utils")
from get_scores import get_scores


def delete_frod(df: pd.DataFrame):
    """Функция удаляет фродовые сценарии"""
    df_frod1 = df.loc[~((df.part_income_for_loan > 0.6) & (df.inf_house == 0 ) & (df.num_doc ==0))].index
    df_frod2 = df.loc[~(df.full_age.isin([23, 24]))].index
    df_frod3 = df.loc[~((df.full_age < 25) & (df.difference_group_minus_personal > 80000))].index
    a = np.intersect1d(df_frod1, df_frod2)
    b = np.intersect1d(a, df_frod3)
    print('shape, before deleting frod', df.shape)
    df_frod = df.loc[b, :].copy()
    print('shape, after deleting frod', df_frod.shape)
    return df_frod


def main(
        input_file_train=r'train_train_cb_sel.csv',
        input_val_val=r'train_val_cb_sel.csv'
):
    train = pd.read_csv(input_file_train, index_col=0)
    X = train.drop(columns='target')
    y = train.target
    # дефолтный таргет
    cat_features = ['inf_house']
    print(train.target.mean())
    # 1. Правило №1 - если на выплату долга уходит более 60% и он не предоставляет документов и информации о доме
    print('rule_1', train.loc[(train.part_income_for_loan > 0.6) & (train.inf_house == 0 ) & (train.num_doc ==0)].target.mean())
    # 2. Если человеку 23 или 24 года - он фродер.
    print('rule_2', train.loc[train.full_age.isin([23, 24])].target.mean())
    # 3. Если человеку меньше 25 лет и он зарабатывает больше людей в своей группе на 80 тыс., то скорее всего он фрод
    print('rule_3', train.loc[(train.full_age < 25) & (train.difference_group_minus_personal > 80000)].target.mean())
    train_frod = delete_frod(train)
    X = train_frod.drop(columns='target')
    y = train_frod.target
    cb_clf = CatBoostClassifier(random_state=42,
                                cat_features=cat_features)
    cb_clf.fit(X, y)
    test = pd.read_csv(input_val_val, index_col=0)
    test_frod = delete_frod(test)
    X_val = test_frod.drop(columns='target')
    y_val = test_frod.target
    get_scores(y_val, cb_clf.predict(X_val), cb_clf.predict_proba(X_val)[:, 1])
    print('Roc_auc с фродом roc_auc_score: 0.7286069410272143')
main()