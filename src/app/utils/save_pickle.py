import pickle

def save_pickle(
        name_model: str,
        model,
):
    """
    cохраняет модель в pickle файл
    """
    with open(f'{name_model}.pkl', "wb") as f:
        pickle.dump(model, f)