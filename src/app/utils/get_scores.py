from sklearn import metrics

def get_scores(
        y_truely,
        y_predict,
        y_predict_proba
):
    """
    Выводит на печать основные метрики качества модели: precision, recall, f_1, accuracy, ROC_AUC_SCORE
    :param y_truely - настоящие значения таргета:
    :param y_predict - предсказываемые значения таргета:
    :param y_predict_proba - предсказываемые значения вероятности:
    :return:
    """
    print(f' precision: {metrics.precision_score(y_truely, y_predict)}')
    print(f' recall : {metrics.recall_score(y_truely, y_predict)}')
    print(f' f1_score: {metrics.f1_score(y_truely, y_predict)}')
    print(f' accuracy_score: {metrics.accuracy_score(y_truely, y_predict)}')
    print(f' roc_auc_score: {metrics.roc_auc_score(y_truely, y_predict_proba)}')
