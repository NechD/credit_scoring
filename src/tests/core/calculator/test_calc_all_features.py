from src.app.core.calculator import Calculator
from src.app.core.api import Features
import pytest


class TestCalculator(object):
    """Тесты калькулятора"""

    @pytest.mark.parametrize(
        'full_age, inf_house, num_doc, expected_amount',
        [
            # Изменяем возраст, наличие информации о доме и число предоставленных документов
            (26, 1, 3, 200000),
            (60, 1, 6, 200000),
            (26, 0, 3, 150000),
            (60, 0, 6, 150000),
            (26, 0, 0, 125000),
            (60, 1, 2, 125000),
            (26, 0, 0, 125000),
            (60, 1, 2, 125000),
            # вторая ветка
            (18, 1, 3, 125000),
            (25, 1, 10, 125000),
            (18, 0, 3, 100000),
            (25, 0, 10, 100000),
            (18, 0, 3, 100000),
            (25, 0, 10, 100000),
            (18, 1, 2, 50000),
            (25, 0, 0, 50000),
            # очень большой возраст
            (61, 1, 2, 25000),
            (61, 0, 2, 25000),
            (61, 1, 10, 25000),
            (61, 0, 10, 25000),
        ]
    )
    def test_calc_amount2(
            self,
            full_age,
            inf_house,
            num_doc,
            expected_amount,
    ):
        features = Features(
            full_age=int(full_age),
            inf_house=inf_house,
            num_doc=num_doc,
        )
        calculator = Calculator()
        assert calculator.calc_amount2(
            1,
            features,
        ) == expected_amount
